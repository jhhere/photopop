// GRAB THE PACKAGES/VARIABLES WE NEED

var express = require('express');
var app = express();
var ig = require('instagram-node').instagram();

// CONFIGURE THE APP 

// set the port of our application
// procss.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// Tell node where to look for site resources
app.use(express.static(__dirname + '/public'));

// set the view engine to ejs
app.set('view engine', 'ejs');

// Configure instagram app with client-id

// SET THE ROUTES

// set home page route - our profile's images
app.get('/', function(req, res) {

    // use the instagram package to get our profile's media
    // render the home page and pass in the our profile's images
    res.render('pages/index');

});

// START THE SERVER

app.listen(port, function() {
    console.log('App started! Running on http://localhost' + port);

});